package Haruethai;

public class Lab9 {
	public static void main(String args[]) {
        for (int count = 1; count <= 20; count++) {
            if (count == 11) 
                continue; 
                System.out.printf("%d ", count); 
        }
        System.out.println("\nUsed continue to skip printing 11");
    }
}
